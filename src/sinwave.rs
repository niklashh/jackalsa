use alsa::pcm::{Access, Format, HwParams, State, PCM};
use alsa::{Direction, ValueOr};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let pcm = PCM::new("hw:He", Direction::Playback, false)?;
    let hwp = HwParams::any(&pcm)?;
    hwp.set_channels(1)?;
    hwp.set_rate(44100, ValueOr::Nearest)?;
    hwp.set_format(Format::s16()).unwrap();
    hwp.set_access(Access::RWInterleaved).unwrap();
    // hwp.set_buffer_size(256)?;
    // hwp.set_period_size(256 / 4, alsa::ValueOr::Nearest)?;
    pcm.hw_params(&hwp)?;

    let io = pcm.io_i16()?;

    // Make sure we don't start the stream too early
    let hwp = pcm.hw_params_current()?;
    let swp = pcm.sw_params_current()?;
    swp.set_start_threshold(hwp.get_buffer_size()?)?;
    pcm.sw_params(&swp)?;

    // Make a sine wave
    let mut buf = [0; 1024];
    for (i, a) in buf.iter_mut().enumerate() {
        *a =
            ((i as f32 * 2.0 * ::std::f32::consts::PI / 128.0).sin() * i16::MAX as f32 / 4.0) as i16
    }

    // Play it back for 2 seconds.
    for _ in 0..2 * 44100 / 1024 {
        assert_eq!(io.writei(&buf[..])?, 1024);
        println!("block");
    }

    // In case the buffer was larger than 2 seconds, start the stream manually.
    if pcm.state() != State::Running {
        pcm.start().unwrap()
    };
    // Wait for the stream to finish playback.
    pcm.drain().unwrap();

    Ok(())
}
