pub mod cli;

use alsa::pcm::{Access, Format, HwParams, PCM};
use alsa::{Direction, ValueOr};
use dasp::{signal::window::hanning, Sample};
use itertools::Itertools;
use samplerate::{convert, ConverterType};

use std::sync::mpsc;
use std::sync::{Arc, Mutex};

const BUF_SIZE: usize = 1024; // same as jack
const NUM_CHANNELS: usize = 2;
const ALSA_PERIOD_SIZE: i64 = BUF_SIZE as i64;
const ALSA_NUM_PERIODS: i64 = 2;
const STATIC_RESAMPLE_FACTOR: f64 = 1.0;
const SMOOTH_SIZE: usize = 256;
const CATCH_FACTOR: f64 = 100000.0;
const CATCH_FACTOR2: f64 = 10000.0;
const TARGET_DELAY: i64 = ALSA_NUM_PERIODS * ALSA_PERIOD_SIZE / 2 - BUF_SIZE as i64 / 2;
const MAX_DIFF: i64 = TARGET_DELAY;
const PCLAMP: f64 = 5.0;
const CONTROLQUANT: f64 = 10000.0;
const RESAMPLE_LOWER_LIMIT: f64 = 0.25;
const RESAMPLE_UPPER_LIMIT: f64 = 4.0;
const RESPONSIVE_FACTOR: f64 = 10.0; // how quickly smooth_offset affects the resampling

pub fn alsa_out() -> Result<(), Box<dyn std::error::Error>> {
    let (ctrlctx, ctrlcrx) = mpsc::channel();
    ctrlc::set_handler(move || {
        eprintln!(" Quitting...");
        let _ = ctrlctx.send(());
    })
    .expect("Error setting Ctrl-C handler");

    let sinkbuf1 = Arc::new(Mutex::new([[0f32; BUF_SIZE]; NUM_CHANNELS]));
    let sinkbuf2 = Arc::clone(&sinkbuf1);
    let (tx, rx) = mpsc::channel();

    let handle = std::thread::spawn(move || {
        loop {
            let p = PCM::new("hw:He", Direction::Playback, false).unwrap();

            {
                let hwp = HwParams::any(&p).unwrap();
                let channels = NUM_CHANNELS as u32;
                let rate = 44100;
                let format = Format::s16();
                let access = Access::MMapInterleaved;
                hwp.set_channels(channels).unwrap();
                hwp.set_rate(rate, ValueOr::Nearest).unwrap();
                hwp.set_format(format).unwrap();
                hwp.set_access(access).unwrap();
                hwp.set_buffer_size(ALSA_PERIOD_SIZE * ALSA_NUM_PERIODS)
                    .unwrap();
                hwp.set_period_size(ALSA_PERIOD_SIZE, ValueOr::Nearest)
                    .unwrap();
                p.hw_params(&hwp).unwrap();
            }

            {
                let hwp = p.hw_params_current().unwrap();
                let swp = p.sw_params_current().unwrap();
                let (bufsize, periodsize) = (
                    hwp.get_buffer_size().unwrap(),
                    hwp.get_period_size().unwrap(),
                );
                // Make sure we don't start the stream too early
                swp.set_start_threshold(bufsize - periodsize).unwrap();
                swp.set_avail_min(periodsize).unwrap();
                p.sw_params(&swp).unwrap();
                println!(
                    "Opened audio output hw:He with parameters: {:?}, {:?}",
                    hwp, swp
                );
            }
            //  let io_playback = p.io_i16().unwrap();
            let mut mmap = p.direct_mmap_playback::<i16>();

            let mut offset_integral = 0f64;
            let mut resample_mean = 1f64;
            let mut offset_array = [0i64; SMOOTH_SIZE];
            let mut offset_differential_index = 0;
            let hann = hanning::<f64>(SMOOTH_SIZE)
                .take(SMOOTH_SIZE)
                .collect::<Vec<f64>>();

            let mut tmpbuf = [0f32; NUM_CHANNELS * BUF_SIZE];
            let mut resamplebuf = [0f32; NUM_CHANNELS * BUF_SIZE * RESAMPLE_UPPER_LIMIT as usize];

            while let Err(mpsc::TryRecvError::Empty) = ctrlcrx.try_recv() {
                let frames = rx.recv().unwrap();
                let roundtimer = std::time::Instant::now();
                let [ref in_l, ref in_r] = *sinkbuf1.lock().unwrap();

                if let Ok(ref mut mmap) = mmap {
                    let avail = mmap.avail();

                    let mut delay = ALSA_NUM_PERIODS * ALSA_PERIOD_SIZE
                        - avail
                        - (frames as f64 * STATIC_RESAMPLE_FACTOR) as i64;

                    if delay > TARGET_DELAY + MAX_DIFF {
                        println!("Overrun");

                        offset_integral = -(resample_mean - STATIC_RESAMPLE_FACTOR)
                            * CATCH_FACTOR
                            * CATCH_FACTOR2;
                        offset_array = [0; SMOOTH_SIZE];
                    } else if delay < TARGET_DELAY - MAX_DIFF {
                        while delay < TARGET_DELAY {
                            let to_write = if (TARGET_DELAY - delay) > 512 {
                                512
                            } else {
                                TARGET_DELAY - delay
                            };

                            let wrote = mmap.write(
                                &mut std::iter::repeat(0i16).take(to_write as usize * NUM_CHANNELS),
                            );
                            println!("Underrun compensation");
                            dbg!(to_write, delay, wrote);
                            delay += wrote;
                        }

                        delay = TARGET_DELAY;

                        offset_integral = -(resample_mean - STATIC_RESAMPLE_FACTOR)
                            * CATCH_FACTOR
                            * CATCH_FACTOR2;
                        offset_array = [0; SMOOTH_SIZE];
                    }

                    let offset = delay - TARGET_DELAY;

                    // save the offset
                    offset_array[offset_differential_index] = offset;

                    let mut smooth_offset = 0.0;
                    for i in 0..SMOOTH_SIZE {
                        smooth_offset += offset_array[(i + offset_differential_index) % SMOOTH_SIZE]
                            as f64
                            * hann[i];
                    }
                    smooth_offset /= SMOOTH_SIZE as f64;
                    offset_integral += smooth_offset;

                    offset_differential_index += 1;
                    offset_differential_index %= SMOOTH_SIZE;

                    if smooth_offset.abs() < PCLAMP {
                        smooth_offset = 0.0;
                    }

                    // ok. now this is the PI controller.
                    // u(t) = K * ( e(t) + 1/T \int e(t') dt' )
                    // K = 1/CATCH_FACTOR and T = CATCH_FACTOR2

                    let mut current_resample_factor = ((STATIC_RESAMPLE_FACTOR
                        - (smooth_offset + offset_integral / CATCH_FACTOR2) / CATCH_FACTOR
                        - resample_mean)
                        * CONTROLQUANT
                        + 0.5)
                        .floor()
                        / CONTROLQUANT
                        + resample_mean;

                    if current_resample_factor < RESAMPLE_LOWER_LIMIT {
                        current_resample_factor = RESAMPLE_LOWER_LIMIT;
                    } else if current_resample_factor > RESAMPLE_UPPER_LIMIT {
                        current_resample_factor = RESAMPLE_UPPER_LIMIT;
                    };

                    resample_mean = (1.0 - 1.0 / RESPONSIVE_FACTOR) * resample_mean
                        + (1.0 / RESPONSIVE_FACTOR) * current_resample_factor;

                    for (i, s) in in_l.iter().interleave(in_r).enumerate() {
                        tmpbuf[i] = *s;
                    }

                    let resampled = convert(
                        44100,
                        (44100.0 * current_resample_factor) as u32,
                        NUM_CHANNELS,
                        ConverterType::Linear,
                        &tmpbuf,
                        &mut resamplebuf,
                    )
                    .unwrap();

                    let mut data = resamplebuf
                        .iter()
                        .take(resampled)
                        .map(std::borrow::ToOwned::to_owned)
                        .map(f32::to_sample::<i16>);

                    mmap.write(&mut data);

                    // println!(
                    //     "res:{:7.4}, diff ={:7.0}, int ={:7.0}, offset ={:4}, roundtime ={:.1?}",
                    //     current_resample_factor,
                    //     smooth_offset,
                    //     offset_integral,
                    //     offset,
                    //     roundtimer.elapsed()
                    // );

                    use alsa::pcm::State;
                    match mmap.status().state() {
                        State::Running => {}
                        State::Prepared => {
                            println!("Starting audio output stream");
                            p.start().unwrap();
                        }
                        State::XRun => {
                            println!("Underrun in audio output stream!");
                            p.prepare().unwrap();
                        }
                        State::Suspended => {
                            println!("Resuming audio output stream");
                            let resume = p.resume();
                            if let Err(e) = resume {
                                eprintln!("PCM resume failed: {:?}\nRestarting...", e);
                                break;
                            };
                        }
                        State::Disconnected => {
                            println!("Alsa device was disconnected");
                            break;
                        }
                        s => {
                            eprintln!("Unexpected pcm state {:?}", s);
                        }
                    }
                } else {
                    println!("mmap err: {:?}", mmap);
                }
            }
        }
    });

    let (client, status) =
        jack::Client::new("jackalsa", jack::ClientOptions::NO_START_SERVER).unwrap();

    assert!(status.is_empty());

    let in_l = client
        .register_port("in_l", jack::AudioIn::default())
        .unwrap();
    let in_r = client
        .register_port("in_r", jack::AudioIn::default())
        .unwrap();
    let name_in_l = in_l.name().unwrap();
    let name_in_r = in_r.name().unwrap();

    let process_callback = move |_: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
        let in_l_slice = in_l.as_slice(ps);
        let in_r_slice = in_r.as_slice(ps);

        {
            let mut mem = sinkbuf2.lock().unwrap();
            mem[0].clone_from_slice(in_l_slice);
            mem[1].clone_from_slice(in_r_slice);
        }
        tx.send(ps.frames_since_cycle_start() as usize).unwrap();

        jack::Control::Continue
    };
    let process = jack::ClosureProcessHandler::new(process_callback);

    // Activate the client, which starts the processing.
    let client = client.activate_async((), process).unwrap();

    // let jack_sinks = client.as_client().ports(
    //     Some("PulseAudio JACK Sink"),
    //     None,
    //     jack::PortFlags::IS_OUTPUT,
    // );
    // assert_eq!(jack_sinks.len(), 2);

    let jack_sinks =
        client
            .as_client()
            .ports(Some("jack_config_client"), None, jack::PortFlags::IS_OUTPUT);
    assert_eq!(jack_sinks.len(), 2);

    client
        .as_client()
        .connect_ports_by_name(&jack_sinks[0], &name_in_l)
        .unwrap();
    client
        .as_client()
        .connect_ports_by_name(&jack_sinks[1], &name_in_r)
        .unwrap();

    let _ = handle.join();

    match client.deactivate() {
        Ok(_) => (),
        Err(err) => eprintln!("Error deactivating client: {:?}. Is jack still alive?", err),
    };

    Ok(())
}
