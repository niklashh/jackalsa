use jackalsa::alsa_out;
use jackalsa::cli::Cli;
use structopt::StructOpt;

fn main() {
    let args = Cli::from_args();

    match args {
        Cli::In => println!("alsa_in"),
        Cli::Out => {
            alsa_out().unwrap();
        }
    }
}
