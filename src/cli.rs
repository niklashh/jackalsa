use structopt::StructOpt;

#[derive(StructOpt)]
pub enum Cli {
    In,
    Out,
}
